#!/bin/bash

# Run this script as root on a linux machine to
# block (at least some) facebook trackers

echo "0.0.0.0   www.facebook.com" >> /etc/hosts
echo "0.0.0.0   facebook.com" >> /etc/hosts
echo "0.0.0.0   login.facebook.com" >> /etc/hosts
echo "0.0.0.0   www.login.facebook.com" >> /etc/hosts
echo "0.0.0.0   fbcdn.net" >> /etc/hosts
echo "0.0.0.0   www.fbcdn.net" >> /etc/hosts
echo "0.0.0.0   fbcdn.com" >> /etc/hosts
echo "0.0.0.0   www.fbcdn.com" >> /etc/hosts
echo "0.0.0.0   static.ak.fbcdn.net" >> /etc/hosts
echo "0.0.0.0   static.ak.connect.facebook.com" >> /etc/hosts
echo "0.0.0.0   connect.facebook.net" >> /etc/hosts
echo "0.0.0.0   www.connect.facebook.net" >> /etc/hosts
echo "0.0.0.0   meta.com" >> /etc/hosts
echo "0.0.0.0   www.meta.com" >> /etc/hosts
echo "0.0.0.0   apps.facebook.com" >> /etc/hosts
